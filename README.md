# php-fpm Dockerimages

## Build image

Build basic

    docker build --pull --target base --build-arg PHP_VERSION=8.2 -t php-fpm:8.2 .

Build basic xdebug

    docker build --pull --target xdebug --build-arg PHP_VERSION=8.2 -t php-fpm:8.2-xdebug .

Build with buildx mulitarch

    docker buildx create --name laemmibuilder --bootstrap --use
    docker buildx inspect laemmibuilder
    docker buildx build --push --target base --platform linux/amd64,linux/arm64 --build-arg PHP_VERSION=8.2 -t php-fpm:8.2 .
    docker buildx build --push --target xdebug --platform linux/amd64,linux/arm64 --build-arg PHP_VERSION=8.2 -t php-fpm:8.2-xdebug .
    docker buildx rm -f laemmibuilder


    docker buildx build --platform linux/amd64,linux/arm64,linux/arm64/v8 --build-arg PHP_VERSION=8.2 -t php-fpm:8.2 .

## Test image

    docker run -it --rm php-fpm:8.2 bash

## Interested links

- https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker-executor
- https://devopscube.com/run-docker-in-docker/

## Requirements for runner plattform

    sudo apt-get install -y qemu qemu-user-static