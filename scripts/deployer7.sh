#!/bin/sh

set -euf -o pipefail

composer global req deployer/deployer:^7.2
ln -s ~/.composer/vendor/bin/dep /usr/local/bin/dep
