#!/bin/sh

set -euf -o pipefail

wget https://downloads.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz \
  && tar xfz ioncube_loaders_lin_x86-64.tar.gz