ARG PHP_VERSION=8.3
FROM php:${PHP_VERSION}-fpm-alpine as base

# Metadata
LABEL maintainer="laemmi@spacerabbit.de"

WORKDIR /tmp

# Copy scripts
COPY --chmod=755 ./scripts/* /tmp/

# Install
RUN ./composer.sh \
    && ./deployer7.sh \
    && ./php_ini.sh \
#
# Packages
    && apk add --no-cache $PHPIZE_DEPS \
    bash \
    freetype-dev \
    git \
    icu-dev \
    jq \
#    imap-dev \
    libjpeg-turbo-dev \
    libpng-dev \
    libwebp-dev \
    libxpm-dev \
    libxslt-dev \
    libzip-dev \
    linux-headers \
    mariadb-connector-c \
    mysql-client \
    nodejs \
    npm \
    oniguruma-dev \
    openldap-dev \
    openssh \
    rsync \
    yarn \
    zip \
#
# PHP extensions configure
    && docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp --with-xpm \
#    && docker-php-ext-configure imap --with-imap --with-imap-ssl \
    && docker-php-ext-configure ldap --with-libdir=lib/ \
    && docker-php-ext-configure zip \
#
# PHP extensions install
    && docker-php-ext-install -j "$(nproc)" \
    bcmath \
    exif \
    gd \
#    imap \
    intl \
    ldap \
    pcntl \
    pdo \
    pdo_mysql \
    mbstring \
    mysqli \
    opcache \
    soap \
    sockets \
    xml \
    xsl \
    zip \
    && docker-php-source delete \
#
# PHP pecl extensions install
    && pecl install \
    redis \
#
# PHP extensions enable
    && docker-php-ext-enable \
    gd \
    intl \
    mbstring \
    redis \
#
# Cleanup
    && /tmp/cleanup.sh

WORKDIR /var/www/html

########################################################################################################################

FROM base AS xdebug
ARG PHP_VERSION

# Metadata
LABEL maintainer="laemmi@spacerabbit.de"

# Copy scripts
COPY --chmod=755 ./scripts/cleanup.sh /tmp/

# PHP xdebug
RUN  \
    if [ "${PHP_VERSION}" = "7.4" ]; then \
    pecl install xdebug-3.1.6; \
    elif [ "${PHP_VERSION}" = "8.3" ]; then \
    pecl install xdebug-3.3.0alpha3; \
    else \
    pecl install xdebug; \
    fi \
    && docker-php-ext-enable xdebug \
    && echo "xdebug.mode=develop,debug" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.client_host=host.docker.internal" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.start_with_request=1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
#
# Cleanup
    && /tmp/cleanup.sh

WORKDIR /var/www/html
